// Fill out your copyright notice in the Description page of Project Settings.


#include "TSWheeledVehicle.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/SkeletalMesh.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/InputComponent.h"
#include "Components/WidgetComponent.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Blueprint/UserWidget.h"
#include "Math/UnrealMathUtility.h"
#include "TSVehicleWheel.h"

ATSWheeledVehicle::ATSWheeledVehicle() {
    static ConstructorHelpers::FObjectFinder<USkeletalMesh> CarMesh(TEXT("/Game/Car/mclaren/mclaren.mclaren")); 
    GetMesh()->SetSkeletalMesh(CarMesh.Object);

	InCarCameraPos = CreateDefaultSubobject<USceneComponent>(TEXT("InCarCameraPos"));
	InCarCameraPos->SetupAttachment(RootComponent);
	InCarCameraPos->SetRelativeLocation(FVector(-4.0f, -28.0f, 20.0f));
	InCarCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("InCarCamera"));
	InCarCamera->SetupAttachment(InCarCameraPos);
	InCarCamera->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	InCarCamera->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	InCarCamera->FieldOfView = 55.0f;

	ChaseSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("ChaseSpringArm "));
	ChaseSpringArm->SetupAttachment(RootComponent);
	ChaseSpringArm->SetRelativeLocation(FVector(-180.0f, 0.0f, 0.0f));
	ChaseSpringArm->SetRelativeRotation(FRotator(-5.0f, 0.0f, 0.0f));
	ChaseSpringArm->SocketOffset = FVector(0.0f, 0.0f, 100.0f);
	ChaseSpringArm->TargetArmLength = 800.0f;
	ChaseCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ChaseCamera"));
	ChaseCamera->SetupAttachment(ChaseSpringArm);
	ChaseCamera->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	ChaseCamera->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	ChaseCamera->FieldOfView = 55.0f;

	UWheeledVehicleMovementComponent4W* Vehicle4W = CastChecked<UWheeledVehicleMovementComponent4W>(GetVehicleMovement());
	check(Vehicle4W->WheelSetups.Num() == 4);
	Vehicle4W->WheelSetups[0].WheelClass = UTSVehicleWheel::StaticClass();
	Vehicle4W->WheelSetups[0].BoneName = FName("right_front_bone");
	Vehicle4W->WheelSetups[1].WheelClass = UTSVehicleWheel::StaticClass();
	Vehicle4W->WheelSetups[1].BoneName = FName("left_front_bone");
	Vehicle4W->WheelSetups[2].WheelClass = UTSVehicleWheel::StaticClass();
	Vehicle4W->WheelSetups[2].BoneName = FName("right_rear_bone");
	Vehicle4W->WheelSetups[3].WheelClass = UTSVehicleWheel::StaticClass();
	Vehicle4W->WheelSetups[3].BoneName = FName("left_rear_bone");

	Dashboard = CreateDefaultSubobject<UWidgetComponent>(TEXT("Dashboard"));
	Dashboard->SetupAttachment(RootComponent);
	Dashboard->SetRelativeLocation(FVector(54.7f, -27.0f, 8.9f));
	Dashboard->SetRelativeRotation(FRotator(-12.0f, 180.0f, 0.0f));
	Dashboard->SetRelativeScale3D(FVector(0.024f, 0.024f, 0.024f));
	UClass* Widget = LoadClass<UUserWidget>(NULL, TEXT("WidgetBlueprint'/Game/UI/Dashboard.Dashboard'"));
	Dashboard->SetWidgetClass(Widget);

	static ConstructorHelpers::FObjectFinder<USoundCue> EngineSoundCueFinder(TEXT("SoundCue'/Game/Sound/SoundCue_Engine.SoundCue_Engine'"));
	EngineSoundCue = EngineSoundCueFinder.Object;
	EngineSoundPlayer = CreateDefaultSubobject<UAudioComponent>(TEXT("Engine Sound Player"));
	EngineSoundPlayer->SetupAttachment(RootComponent);
	EngineSoundPlayer->SetRelativeLocation(FVector(0.0f, 0.0f, -33.0f));
	EngineSoundPlayer->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	if (EngineSoundCue != nullptr && EngineSoundCue->IsValidLowLevelFast()) {
		EngineSoundPlayer->SetSound(EngineSoundCue);
	}

	static ConstructorHelpers::FObjectFinder<USoundCue> FetalRingCueFinder(TEXT("SoundCue'/Game/Sound/SoundCue_FetalRing.SoundCue_FetalRing'"));
	FetalRingCue = FetalRingCueFinder.Object;
	FetalRingPlayer = CreateDefaultSubobject<UAudioComponent>(TEXT("Fetal Ring Player"));
	FetalRingPlayer->SetupAttachment(RootComponent);
	FetalRingPlayer->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	FetalRingPlayer->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	if (FetalRingCue != nullptr && FetalRingCue->IsValidLowLevelFast()) {
		FetalRingPlayer->SetSound(FetalRingCue);
		FetalRingPlayer->Stop();
	}
}

void ATSWheeledVehicle::BeginPlay() {
	Super::BeginPlay();
	SwitchCamera();

	if (EngineSoundCue) {
		EngineSoundCue->PitchMultiplier = 0.1f;
	}
	if (EngineSoundPlayer->IsPlaying()) {
		EngineSoundPlayer->Play();
	}
}

void ATSWheeledVehicle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ATSWheeledVehicle::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATSWheeledVehicle::MoveRight);
	PlayerInputComponent->BindAxis("Lookup", this, &ATSWheeledVehicle::Lookup);
	PlayerInputComponent->BindAxis("Turn", this, &ATSWheeledVehicle::Turn);
	PlayerInputComponent->BindAction("SwitchCamera", IE_Pressed, this, &ATSWheeledVehicle::SwitchCamera);
	PlayerInputComponent->BindAction("Handbrake", IE_Pressed, this, &ATSWheeledVehicle::HandbrakePressed);
	PlayerInputComponent->BindAction("Handbrake", IE_Released, this, &ATSWheeledVehicle::HandbrakeReleased);
}

void ATSWheeledVehicle::MoveForward(float Val) {
	GetVehicleMovement()->SetThrottleInput(Val);
	UpdateEngineSoundPitch();
	ProcessFetalRing();
}

void ATSWheeledVehicle::MoveRight(float Val) {
	GetVehicleMovement()->SetSteeringInput(Val);
}

void ATSWheeledVehicle::HandbrakePressed() {
	GetVehicleMovement()->SetHandbrakeInput(true);
	//UWheeledVehicleMovementComponent* MovementComponent = GetVehicleMovement();
	//check(MovementComponent);
	//if (MovementComponent->GetForwardSpeed() > 2.0f) {
	if (!FetalRingPlayer->IsPlaying()) {
		FetalRingPlayer->Play();
	}
	//}
}

void ATSWheeledVehicle::HandbrakeReleased() {
	GetVehicleMovement()->SetHandbrakeInput(false);
	if (FetalRingPlayer->IsPlaying()) {
		FetalRingPlayer->Stop();
	}
}

void ATSWheeledVehicle::Lookup(float Val) {

}

void ATSWheeledVehicle::Turn(float Val) {

}

void ATSWheeledVehicle::SwitchCamera() {
	bool isChaseCamera = ChaseCamera->IsActive();
	ChaseCamera->SetActive(!isChaseCamera);
	InCarCamera->SetActive(isChaseCamera);
	ChaseCamera->SetVisibility(!isChaseCamera);
	InCarCamera->SetVisibility(isChaseCamera);
}

void ATSWheeledVehicle::ConfigureWheels() {
	//getWheelData
	//	getTireData
}

void ATSWheeledVehicle::UpdateEngineSoundPitch() {
	UWheeledVehicleMovementComponent* MovementComponent = GetVehicleMovement();
	check(MovementComponent);
	if (EngineSoundCue) {
		EngineSoundCue->PitchMultiplier = 0.1f + 2.0f * (MovementComponent->GetEngineRotationSpeed() / MovementComponent->GetEngineMaxRotationSpeed());
	}
}

void ATSWheeledVehicle::ProcessFetalRing() {
	if (FMath::IsNearlyEqual(GetVelocity().Rotation().Yaw, RootComponent->GetForwardVector().Rotation().Yaw)) {
		FetalRingPlayer->Stop();
	}
	else if (!FetalRingPlayer->IsPlaying()){
		FetalRingPlayer->Play();
	}
}

bool ATSWheeledVehicle::ReplaceEngine(FString EngineName) {
	return false;
}

bool ATSWheeledVehicle::ReplaceTransmission(FString TransmissionName) {
	return false;
}

bool ATSWheeledVehicle::ReplaceDifferential(FString DifferentialName) {
	return false;
}
