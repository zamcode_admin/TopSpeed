// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "TSVehicleWheel.generated.h"

/**
 * 
 */
UCLASS()
class TOPSPEED_API UTSVehicleWheel : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UTSVehicleWheel();
	//DECLARE_DELEGATE_OneParam(FOnConfigure, UTSVehicleWheel*);

public:
	//static FOnConfigure ConfigureDelegate;
};
