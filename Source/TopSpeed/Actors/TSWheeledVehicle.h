// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "Engine/DataTable.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "TSWheeledVehicle.generated.h"

class UCameraComponent;
class USpringArmComponent;
class USceneComponent;
class UInputComponent;

/**
 *
 */
USTRUCT()
struct TOPSPEED_API FTFCarConfigure : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	FString CarName;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	FString Brand;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	FString Engine;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	FString Transmission;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	FString Differential;

	//FString Brake;
	//FString Steering;
	//FString Avoidance;
};

USTRUCT()
struct TOPSPEED_API FTSEngineConfigure : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	FRuntimeFloatCurve TorqueCurve;

	UPROPERTY(EditAnywhere, Category = Setup, meta = (ClampMin = "0.01", UIMin = "0.01"))
	float MaxRPM;

	//UPROPERTY(EditAnywhere, Category = Setup, meta = (ClampMin = "0.01", UIMin = "0.01"))
	//float MOI;

	//UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	//float DampingRateFullThrottle;

	//UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	//float DampingRateZeroThrottleClutchEngaged;

	//UPROPERTY(EditAnywhere, Category = Setup, AdvancedDisplay, meta = (ClampMin = "0.0", UIMin = "0.0"))
	//float DampingRateZeroThrottleClutchDisengaged;

	// turbo
	// Mechanical pressurization
};

USTRUCT()
struct TOPSPEED_API FTSTransmissionConfigure : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	bool bUseGearAutoBox;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float GearSwitchTime;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float GearAutoBoxLatency;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float FinalRatio;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	TArray<FVehicleGearData> ForwardGears;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float ReverseGearRatio;
	//float NeutralGearUpRatio;
	//float ClutchStrength;
};

USTRUCT()
struct TOPSPEED_API FTSDifferentialConfigure : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	TEnumAsByte<EVehicleDifferential4W::Type> DifferentialType;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float FrontRearSplit;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float FrontLeftRightSplit;

	UPROPERTY(EditAnywhere, Category = CarConfigure)
	float RearLeftRightSplit;

	float CentreBias;
	float FrontBias;
	float RearBias;
};

/**
 * 
 */
UCLASS()
class TOPSPEED_API ATSWheeledVehicle : public AWheeledVehicle
{
	GENERATED_BODY()

public:
	ATSWheeledVehicle();

public:

private:
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void BeginPlay() override;

	void MoveForward(float Val);
	void MoveRight(float Val);
	void Lookup(float Val);
	void Turn(float Val);
	void HandbrakePressed();
	void HandbrakeReleased();
	void SwitchCamera();

	void ConfigureWheels();
	void UpdateEngineSoundPitch();
	void ProcessFetalRing();

	bool ReplaceEngine(FString EngineName);
	bool ReplaceTransmission(FString TransmissionName);
	bool ReplaceDifferential(FString DifferentialName);

private:
	UPROPERTY(EditAnywhere, Category=Camera, meta=(AllowPrivateAccess="true"))
	UCameraComponent* InCarCamera;

	UPROPERTY(EditAnywhere, Category=Camera, meta=(AllowPrivateAccess="true"))
	UCameraComponent* ChaseCamera;

	UPROPERTY(EditAnywhere, Category=Camera, meta=(AllowPrivateAccess="true"))
	USpringArmComponent* ChaseSpringArm;

	UPROPERTY(EditAnywhere, Category=Camera, meta=(AllowPrivateAccess="true"))
	USceneComponent* InCarCameraPos;

	UPROPERTY(EditAnywhere, Category = UI, meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* Dashboard;

	UPROPERTY(EditAnywhere, Category = Sound, meta = (AllowPrivateAccess = "true"))
	class USoundCue* EngineSoundCue;

	UPROPERTY(EditAnywhere, Category = Sound, meta = (AllowPrivateAccess = "true"))
	class UAudioComponent* EngineSoundPlayer;

	UPROPERTY(EditAnywhere, Category = Sound, meta = (AllowPrivateAccess = "true"))
	class USoundCue* FetalRingCue;

	UPROPERTY(EditAnywhere, Category = Sound, meta = (AllowPrivateAccess = "true"))
	class UAudioComponent* FetalRingPlayer;
};
